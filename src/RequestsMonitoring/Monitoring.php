<?php

namespace RequestsMonitoring;

use Predis\Client;

class Monitoring
{
    private static $instance;
    /** @var  Client */
    private $redis;
    private $lifeTime;
    private $start;
    private $end;

    function __construct()
    {

    }

    function __clone()
    {

    }

    public function getInstance()
    {
        if(!self::$instance) {
            self::$instance = new Monitoring();
            self::$instance->redis = self::$instance->getRedis();
            self::$instance->lifeTime = 86400;
        }

        return self::$instance;
    }

    protected function getRedis()
    {
        try {
            \Predis\Autoloader::register();
            return new Client();
        }
        catch (\Exception $e) {
            throw new \Exception("Can't connect to redis, error: " . $e->getMessage());
        }
    }


    public static function start()
    {
        self::getInstance()->start = microtime(true);
    }

    public static function end()
    {
        self::getInstance()->end = microtime(true);
        self::getInstance()->save();
    }

    public static function getStat()
    {
        $stat = [];
        $self = self::getInstance();
        $redis = $self->redis;
        $paths = $self->redis->smembers('paths');
        try {
            foreach ($paths as $path) {
                $pathData = $redis->lrange($path, 0, -1);
                $execTime = 0;
                $total = 0;
                $times = [];
                foreach ($pathData as $hash) {
                    if ($redis->exists($hash)) {
                        $times[] = $redis->hget($hash, 'time');
                        $execTime += $redis->hget($hash, 'execTime');
                        $total++;
                    }
                }
                $averageSeconds = 0;
                $countTimes = count($times);
                if($countTimes > 1) {
                    sort($times);
                    $averageSeconds = $times[$countTimes - 1] - $times['0'];
                }
                $stat[] = [
                    'path' => $path,
                    'sec/req' => $total ? number_format($execTime / $total, 2) : 0,
                    'req/sec' => $averageSeconds ? number_format($total / $averageSeconds, 2) : 0,
                    'requests' => $total,
                ];
            }
        } catch(\Exception $e) {
            echo "\n " . $e->getMessage();
        }

        return $stat;
    }

    protected function save()
    {
        $self = self::getInstance();
        $hash = $self->formHash();
        $path = $self->formPath();
        $execTime = $self->end - $self->start;
//        $self->redis->flushdb();
        // save exectime
        $self->redis->hmset($hash, [
            'execTime' => $execTime,
            'time' => time(),
        ]);
        $self->redis->expireat($hash, time() + $self->lifeTime);
        // adds hash of exectime to the path
        $self->redis->lpush($path, $hash);
        // remove old hashes from list
        $pathData = $self->redis->lrange($path, 0, -1);
        $pathData = array_reverse($pathData);
        foreach ($pathData as $key) {
            if(!$self->redis->exists($key)) {
                // removes last element from the list, it is possible when we have fixed lifeTime
                $self->redis->rpop($path);
            }
        }
        // save path
        $self->redis->sadd('paths', $path);
    }

    /**
     * @return string
     */
    protected function formHash()
    {
        return md5(uniqid(rand(),1));
    }

    /**
     *
     */
    protected function formPath()
    {
        return $_SERVER['REQUEST_SCHEME'] .'://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }
}